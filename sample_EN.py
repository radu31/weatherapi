from weatherapi import *

KEY = ""
obj = Green_weather(KEY, "Montreal", lang="english")
cur = Currently(obj)
min = Minutely(obj)
hou = Hourly(obj)
dai = Daily(obj)

def curr_cast():
    # rain | snow | sleet
    precip = ("none" if cur.get_precip_intensity()==0 else
              str(cur.get_precip_intensity()+"mm/h")
              +cur.get_precip_type()=="rain")
    result = f"""{cur.get_summary()}
    Precipitation: {precip.capitalize()}
    Nearest storm distance {cur.get_nearest_storm()}{cur.get_distance_unit()}
    Apparent temperature: {cur.get_apparent_temperature()}{cur.get_degree_unit()} - Humidity: {(cur.get_humidity()*100):.2f}%
    Wind speed: {(cur.get_wind_speed()*3.6):.2f}{cur.get_speed_unit()} - UV index: {cur.get_uv_index()}
    Cloud cover: {(cur.get_cloud_cover()*100)}% - Visibility: {cur.get_visibility()}{cur.get_distance_unit()}""" #visibility in Km | precipAccumulation in Cm | precipIntensity in Mm/h | cloudCover in [0,1]
    return result

def min_cast(minute_range: int):
    minutely_l = list()
    for minute in range(0, minute_range+1, 15):
        min.set_minute(minute)
        minutely_l.append(f"{min.get_time()} (in {minute} minutes) - {(min.get_precip_probability()):.2f}% with an intensity of {(min.get_precip_intensity()):.2f} mm/h")
    return minutely_l

def hou_cast(hour_range: int):
    hourly_l = list()
    for hour in range(hour_range):
        hou.set_hour(hour)
        result = f"""{hou.get_time()}\n{hou.get_summary()}
Chance of rain: {(hou.get_precip_probability()*100):.2f}% - with an intensity of {(hou.get_precip_intensity()):.2f} mm/h
Temperature: {hou.get_apparent_temperature()}{hou.get_degree_unit()}
Humidity: {(hou.get_humidity()*100):.2f}% - Wind speed: {(hou.get_wind_speed()*3.6):.2f}{hou.get_speed_unit()} - UV index: {hou.get_uv_index()}\n\n"""
        hourly_l.append(result)
    return hourly_l

def dai_cast(day_range: int):
    daily_l = list()
    for day in range(day_range):
        dai.set_day(day)
        result = f"""{dai.get_time()}\n{dai.get_summary()}
Chance of rain: {(dai.get_precip_probability()*100):.2f}% - with an intensity of {(dai.get_precip_intensity()):.2f} mm/h
Highest temperature: {dai.get_apparent_temperature_high()}{dai.get_degree_unit()}
Humidity: {(dai.get_humidity()*100):.2f}% - Wind speed: {(dai.get_wind_speed()*3.6):.2f}{dai.get_speed_unit()} - UV index: {dai.get_uv_index()}\n\n"""
        daily_l.append(result)
    return daily_l

def ale_cast():
    pass

print("Powered by Dark Sky")
print(str(obj.get_location()))

print("Current Cast")
print(curr_cast())

print("\nMinutely Cast")
for minute in min_cast(60):
    print(minute)

print("\nHourly Cast")
for hour in hou_cast(12):
    print(hour)

print("\nDaily Cast")
for day in dai_cast(5):
    print(day)
