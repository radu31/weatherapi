from geopy.geocoders import Nominatim
from datetime import date,datetime,timedelta
from json import load as jload
import sys, requests
#____________________________________________________________________________
#"exclude=currently,minutely,hourly,daily,alerts&units=si"

def farenheit_to_celsius(f):
	c = (f-32)*(5/9)
	return c

def celsius_to_farenheit(c):
	f = (c*(9/5))+32
	return f

def celsius_to_kelvin(c):
	k = c+273.15
	return k

def farenheit_to_kelvin(f):
	k = farenheit_to_celsius(f)+273
	return k

def mi_to_km(mi):
	km = mi*1.609344
	return float("{:.3f}".format(km))

def km_to_mi(km):
	mi = km*0.6213712
	return float("{:.3f}".format(mi))

def lang_json_parser(file:str, language:str):
	try:
		with open(file, 'r') as lan_file:
			lang = jload(lan_file)[language.lower()]
			return lang
	except:
		return "en"

def units_parser(unit):
	units_dict = {
		"auto": "auto",
		"automatic": "auto",
		"canadian": "ca",
		"can": "ca",
		"ca": "ca",
		"uk": "uk2",
		"united kingdom": "uk2",
		"us": "us",
		"usa": "us",
		"united states": "us",
		"imperial": "us",
		"international": "si",
		"metric": "si"}
	try:
		return units_dict[unit]
	except:
		return "si"

class Green_weather:
	def __init__(self, key: str, city : str, units = "metric", lang = "french"):
		self.city = city
		self.unit = units_parser(units)
		self.lang = lang_json_parser("lang.json", lang)

		self.location = Nominatim().geocode(self.city, language="fr_CA") #en_US
		self.latitude = str(self.location.latitude)
		self.longitude = str(self.location.longitude)

		self.address = None
		self.today = None
		self.json_res= None

		self.DARK_SKY_API_KEY = key

	def set_city(self, city):
		self.city = city
		self.location = Nominatim().geocode(self.city, language="fr_CA")

	def get_city(self):
		return self.city

	def get_location(self):
		return self.location

	def get_today(self):
		return self.today
#____________________________________________________________________________
class Currently:
	def __init__(self, obj):
		self.today = date.today()
		self.d_from_date = datetime.strptime(str(self.today), '%Y-%m-%d')
		self.d_to_date = datetime.strptime(str(self.today), '%Y-%m-%d')
		self.delta = self.d_to_date - self.d_from_date
		self.response = requests.get("https://api.darksky.net/forecast/"
									 +obj.DARK_SKY_API_KEY+"/"+obj.latitude
									 +","+obj.longitude+
									 "?exclude=minutely,hourly,daily,alerts&units="+
									 obj.unit+"&lang="+obj.lang)
		self.json_res = self.response.json()

	def get_degree_unit(self):
		return '°F' if self.json_res['flags']['units'] == 'us' else '°C'

	def get_speed_unit(self):
		return 'Mph' if self.json_res['flags']['units'] == 'us' else 'Km/h'

	def get_distance_unit(self):
		return 'Mi' if self.json_res['flags']['units'] == 'us' else 'Km'

	def get_time(self):
		time_t = self.json_res['currently']['time']
		utctimestamp = datetime.utcfromtimestamp(time_t)
		f_time = datetime.strptime(str(utctimestamp), "%Y-%m-%d %H:%M:%S").replace(tzinfo=pytz.utc).astimezone(pytz.timezone('US/Eastern')).strftime('%H:%M')
		return f_time
		# time_t = self.json_res['currently']['time']
		# f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		# return f_time

	def get_summary(self):
		return self.json_res['currently']['summary']

	def get_icon(self):
		return self.json_res['currently']['icon']

	def get_nearest_storm(self):
		return self.json_res['currently']['nearestStormDistance']

	def get_nearest_storm_bearing(self):
		return self.json_res['currently']['nearestStormBearing']

	def get_precip_intensity(self):
		return self.json_res['currently']['precipIntensity']

	def get_precip_probability(self):
		return self.json_res['currently']['precipProbability']

	def get_precip_type(self):
		return self.json_res['currently']['precipType']

	def get_temperature(self):
		return self.json_res['currently']['temperature']

	def get_apparent_temperature(self):
		return self.json_res['currently']['apparentTemperature']

	def get_dew_point(self):
		return self.json_res['currently']['dewPoint']

	def get_humidity(self):
		return self.json_res['currently']['humidity']

	def get_pressure(self):
		return self.json_res['currently']['pressure']

	def get_wind_speed(self):
		return self.json_res['currently']['windSpeed']

	def get_wind_gust(self):
		return self.json_res['currently']['windGust']

	def get_wind_bearing(self):
		return self.json_res['currently']['windBearing']

	def get_cloud_cover(self):
		return self.json_res['currently']['cloudCover']

	def get_uv_index(self):
		return self.json_res['currently']['uvIndex']

	def get_visibility(self):
		return self.json_res['currently']['visibility']

	def get_ozone(self):
		return self.json_res['currently']['ozone']
#____________________________________________________________________________
class Minutely:
	def __init__(self, obj, minute = 0):
		self.days = 1
		self.minute = minute
		self.today = date.today()
		self.d_from_date = datetime.strptime(str(self.today), '%Y-%m-%d')
		self.d_to_date = datetime.strptime(str(date.today() + timedelta(days=1)), '%Y-%m-%d')
		self.delta = self.d_to_date - self.d_from_date
		self.response = requests.get("https://api.darksky.net/forecast/"
									 +obj.DARK_SKY_API_KEY+"/"+obj.latitude
									 +","+obj.longitude+
									 "?exclude=currently,hourly,daily,alerts&units="+
									 obj.unit+"&lang="+obj.lang)
		self.json_res = self.response.json()

	def set_minute(self, minute: int):
		try:
			if minute in range(61):
				self.minute = minute
		except:
			self.minute = 0

	def get_time(self):
		time_t = self.json_res['minutely']['data'][self.minute]['time']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_precip_intensity(self):
		return self.json_res['minutely']['data'][self.minute]['precipIntensity']

	def get_precip_probability(self):
		return self.json_res['minutely']['data'][self.minute]['precipProbability']*100
#____________________________________________________________________________
class Hourly:
	def __init__(self, obj, hour = 0):
		self.days = 2
		self.hour = hour
		self.today = date.today()
		self.d_from_date = datetime.strptime(str(self.today), '%Y-%m-%d')
		self.d_to_date = datetime.strptime(str(date.today() + timedelta(days=2)), '%Y-%m-%d')
		self.delta = self.d_to_date - self.d_from_date
		self.response = requests.get("https://api.darksky.net/forecast/"
									 +obj.DARK_SKY_API_KEY+"/"+obj.latitude
									 +","+obj.longitude+
									 "?exclude=currently,minutely,daily,alerts&units="+
									 obj.unit+"&lang="+obj.lang)
		self.json_res = self.response.json()

	def set_hour(self, hour: int):
		try:
			if hour in range(49):
				self.hour = hour
		except:
			self.hour = 0

	def get_degree_unit(self):
		return '°F' if self.json_res['flags']['units'] == 'us' else '°C'

	def get_speed_unit(self):
		return 'Mph' if self.json_res['flags']['units'] == 'us' else 'Km/h'

	def get_distance_unit(self):
		return 'Mi' if self.json_res['flags']['units'] == 'us' else 'Km'

	def get_time(self):
		time_t = self.json_res['hourly']['data'][self.hour]['time']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_summary(self):
		return self.json_res['hourly']['data'][self.hour]['summary']

	def get_icon(self):
		return self.json_res['hourly']['data'][self.hour]['icon']

	def get_precip_intensity(self):
		return self.json_res['hourly']['data'][self.hour]['precipIntensity']

	def get_precip_probability(self):
		return self.json_res['hourly']['data'][self.hour]['precipProbability']

	def get_precip_type(self):
		return self.json_res['hourly']['data'][self.hour]['precipType']

	def get_temperature(self):
		return self.json_res['hourly']['data'][self.hour]['temperature']

	def get_apparent_temperature(self):
		return self.json_res['hourly']['data'][self.hour]['apparentTemperature']

	def get_dew_point(self):
		return self.json_res['hourly']['data'][self.hour]['dewPoint']

	def get_humidity(self):
		return self.json_res['hourly']['data'][self.hour]['humidity']

	def get_pressure(self):
		return self.json_res['hourly']['data'][self.hour]['pressure']

	def get_wind_speed(self):
		return self.json_res['hourly']['data'][self.hour]['windSpeed']

	def get_wind_gust(self):
		return self.json_res['hourly']['data'][self.hour]['windGust']

	def get_wind_bearing(self):
		return self.json_res['hourly']['data'][self.hour]['windBearing']

	def get_cloud_cover(self):
		return self.json_res['hourly']['data'][self.hour]['cloudCover']

	def get_uv_index(self):
		return self.json_res['hourly']['data'][self.hour]['uvIndex']

	def get_visibility(self):
		return self.json_res['hourly']['data'][self.hour]['visibility']

	def get_ozone(self):
		return self.json_res['hourly']['data'][self.hour]['ozone']
#____________________________________________________________________________
class Daily:
	def __init__(self, obj, day = 0):
		self.days = day
		self.today = date.today()
		self.d_from_date = datetime.strptime(str(self.today), '%Y-%m-%d')
		self.d_to_date = datetime.strptime(str(date.today() + timedelta(days=int(self.days))), '%Y-%m-%d')
		self.delta = self.d_to_date - self.d_from_date
		self.response = requests.get("https://api.darksky.net/forecast/"
									 +obj.DARK_SKY_API_KEY+"/"+obj.latitude
									 +","+obj.longitude+
									 "?exclude=currently,minutely,hourly,alerts&units="+
									 obj.unit+"&lang="+obj.lang)
		self.json_res = self.response.json()

	def set_day(self, day: int):
		try:
			if day in range(8):
				self.days = day
		except:
			self.days = 7

	def get_degree_unit(self):
		return '°F' if self.json_res['flags']['units'] == 'us' else '°C'

	def get_speed_unit(self):
		return 'Mph' if self.json_res['flags']['units'] == 'us' else 'Km/h'

	def get_distance_unit(self):
		return 'Mi' if self.json_res['flags']['units'] == 'us' else 'Km'

	def get_week_summary(self):
		return self.json_res['daily']['summary']

	def get_week_icon(self):
		return self.json_res['daily']['icon']

	def get_time(self):
		time_t = self.json_res['daily']['data'][self.days]['time']
		f_time = datetime.fromtimestamp(time_t).strftime("%A, %d/%m")
		return f_time

	def get_summary(self):
		return self.json_res['daily']['data'][self.days]['summary']

	def get_icon(self):
		return self.json_res['daily']['data'][self.days]['icon']

	def get_sunrise_time(self):
		time_t = self.json_res['daily']['data'][self.days]['sunriseTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_sunset_time(self):
		time_t = self.json_res['daily']['data'][self.days]['sunsetTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_moon_phase(self):
		return self.json_res['daily']['data'][self.days]['moonPhase']

	def get_precip_intensity(self):
		return self.json_res['daily']['data'][self.days]['precipIntensity']

	def get_precip_intensity_max(self):
		return self.json_res['daily']['data'][self.days]['precipIntensityMax']

	def get_precip_intensity_max_time(self):
		time_t = self.json_res['daily']['data'][self.days]['precipIntensityMaxTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_precip_probability(self):
		return self.json_res['daily']['data'][self.days]['precipProbability']

	def get_precip_type(self):
		return self.json_res['daily']['data'][self.days]['precipType']

	def get_temperature_high(self):
		return self.json_res['daily']['data'][self.days]['temperatureHigh']

	def get_temperature_high_time(self):
		time_t = self.json_res['daily']['data'][self.days]['temperatureHighTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_temperature_low(self):
		return self.json_res['daily']['data'][self.days]['temperatureLow']

	def get_temperature_low_time(self):
		time_t = self.json_res['daily']['data'][self.days]['temperatureLowTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_apparent_temperature_high(self):
		return self.json_res['daily']['data'][self.days]['apparentTemperatureHigh']

	def get_apparent_temperature_high_time(self):
		time_t = self.json_res['daily']['data'][self.days]['apparentTemperatureHighTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_apparent_temperature_low(self):
		return self.json_res['daily']['data'][self.days]['apparentTemperatureLow']

	def get_apparent_temperature_low_time(self):
		time_t = self.json_res['daily']['data'][self.days]['apparentTemperatureLowTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_dew_point(self):
		return self.json_res['daily']['data'][self.days]['dewPoint']

	def get_humidity(self):
		return self.json_res['daily']['data'][self.days]['humidity']

	def get_pressure(self):
		return self.json_res['daily']['data'][self.days]['pressure']

	def get_wind_speed(self):
		return self.json_res['daily']['data'][self.days]['windSpeed']

	def get_wind_gust(self):
		return self.json_res['daily']['data'][self.days]['windGust']

	def get_wind_gust_time(self):
		time_t = self.json_res['daily']['data'][self.days]['windGustTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_wind_bearing(self):
		return self.json_res['daily']['data'][self.days]['windBearing']

	def get_cloud_cover(self):
		return self.json_res['daily']['data'][self.days]['cloudCover']

	def get_uv_index(self):
		return self.json_res['daily']['data'][self.days]['uvIndex']

	def get_uv_index_time(self):
		time_t = self.json_res['daily']['data'][self.days]['uvIndexTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_visibility(self):
		return self.json_res['daily']['data'][self.days]['visibility']

	def get_ozone(self):
		return self.json_res['daily']['data'][self.days]['ozone']

	def get_temperature_min(self):
		return self.json_res['daily']['data'][self.days]['temperatureMin']

	def get_temperature_min_time(self):
		time_t = self.json_res['daily']['data'][self.days]['temperatureMinTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_temperature_max(self):
		return self.json_res['daily']['data'][self.days]['temperatureMax']

	def get_temperature_max_time(self):
		time_t = self.json_res['daily']['data'][self.days]['temperatureMaxTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_apparent_temperature_min(self):
		return self.json_res['daily']['data'][self.days]['apparentTemperatureMin']

	def get_apparent_temperature_min_time(self):
		time_t = self.json_res['daily']['data'][self.days]['apparentTemperatureMinTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_apparent_temperature_max(self):
		return self.json_res['daily']['data'][self.days]['apparentTemperatureMax']

	def get_apparent_temperature_max_time(self):
		time_t = self.json_res['daily']['data'][self.days]['apparentTemperatureMaxTime']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time
#____________________________________________________________________________
class Alerts:
	def __init__(self, obj, day = 0):
		try:
			self.days = day
			self.today = date.today()
			self.d_from_date = datetime.strptime(str(self.today), '%Y-%m-%d')
			self.d_to_date = datetime.strptime(str(date.today() + timedelta(days=int(self.days))), '%Y-%m-%d')
			self.delta = self.d_to_date - self.d_from_date
			self.response = requests.get("https://api.darksky.net/forecast/"
										 +obj.DARK_SKY_API_KEY+"/"+obj.latitude
										 +","+obj.longitude+
										 "?exclude=currently,minutely,hourly,daily&units="+
										 obj.unit+"&lang="+obj.lang)
			self.json_res = self.response.json()
		except:
			return "No alerts"

	def get_degree_unit(self):
		return '°F' if self.json_res['flags']['units'] == 'us' else '°C'

	def get_speed_unit(self):
		return 'Mph' if self.json_res['flags']['units'] == 'us' else 'Km/h'

	def get_distance_unit(self):
		return 'Mi' if self.json_res['flags']['units'] == 'us' else 'Km'

	def get_title(self):
		return self.json_res['alerts'][self.days]['title']

	def get_time(self):
		time_t = self.json_res['alerts'][self.days]['time']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_expire_time(self):
		time_t = self.json_res['alerts'][self.days]['expires']
		f_time = datetime.fromtimestamp(time_t).strftime("%H:%M")
		return f_time

	def get_description(self):
		return self.json_res['alerts'][self.days]['description']
