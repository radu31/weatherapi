from weatherapi import *

CLÉ = ""
obj = Green_weather(CLÉ, "Montreal", lang="french")
cur = Currently(obj)
min = Minutely(obj)
hou = Hourly(obj)
dai = Daily(obj)

def curr_cast():
    precip = ("aucune" if cur.get_precip_intensity()==0 else
              str(cur.get_precip_intensity()+"mm/h")+
              ("pluie" if cur.get_precip_type()=="rain" else
               ("neige" if cur.get_precip_type()=="snow" else "verglas")))
    result = f"""{cur.get_summary()}
    Précipitation: {precip.capitalize()}
    Orage le plus proche à {cur.get_nearest_storm()}{cur.get_distance_unit()}
    Température courante: {cur.get_apparent_temperature()}{cur.get_degree_unit()} - Humidité: {(cur.get_humidity()*100):.2f}%
    Vitesse du vent: {(cur.get_wind_speed()*3.6):.2f}{cur.get_speed_unit()} - Index UV: {cur.get_uv_index()}
    Couverture nuageuse: {(cur.get_cloud_cover()*100)}% - Visibilité: {cur.get_visibility()}{cur.get_distance_unit()}""" #visibility en Km | precipAccumulation en Cm | precipIntensity en Mm/h | cloudCover en [0,1]
    return result

def min_cast(minute_range: int):
    minutely_l = list()
    for minute in range(0, minute_range+1, 15):
        min.set_minute(minute)
        minutely_l.append(f"{min.get_time()} (dans {minute} minutes) - {(min.get_precip_probability()):.2f}% avec une intensité de {(min.get_precip_intensity()):.2f} mm/h")
    return minutely_l

def hou_cast(hour_range: int):
    hourly_l = list()
    for hour in range(hour_range):
        hou.set_hour(hour)
        result = f"""{hou.get_time()}\n{hou.get_summary()}
Possibilité de pluie: {(hou.get_precip_probability()*100):.2f}% - avec une intensité de {(hou.get_precip_intensity()):.2f} mm/h
Température: {hou.get_apparent_temperature()}{hou.get_degree_unit()}
Humidité: {(hou.get_humidity()*100):.2f}% - Vitesse du vent: {(hou.get_wind_speed()*3.6):.2f}{hou.get_speed_unit()} - index UV: {hou.get_uv_index()}\n\n"""
        hourly_l.append(result)
    return hourly_l

def dai_cast(day_range: int):
    daily_l = list()
    for day in range(day_range):
        dai.set_day(day)
        result = f"""{dai.get_time()}\n{dai.get_summary()}
Possibilité de pluie: {(dai.get_precip_probability()*100):.2f}% - avec une intensité de {(dai.get_precip_intensity()):.2f} mm/h
Plus haute température: {dai.get_apparent_temperature_high()}{dai.get_degree_unit()}
Humidité: {(dai.get_humidity()*100):.2f}% - Vitesse du vent: {(dai.get_wind_speed()*3.6):.2f}{dai.get_speed_unit()} - index UV: {dai.get_uv_index()}\n\n"""
        daily_l.append(result)
    return daily_l

def ale_cast():
    pass

print("Fait avec Dark Sky - Powered by Dark Sky")
print(str(obj.get_location()))

print("Météo courante - Current Cast")
print(curr_cast())

print("\nMétéo par minute - Minutely Cast")
for minute in min_cast(60):
    print(minute)

print("\nMétéo horaire - Hourly Cast")
for hour in hou_cast(12):
    print(hour)

print("\nMétéo quotidienne - Daily Cast")
for day in dai_cast(5):
    print(day)
